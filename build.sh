#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        DEBUGFLAG="-ggdb -fno-stack-protector"
    else
        DEBUGFLAG="-fomit-frame-pointer -fno-stack-protector"
    fi

    if [ -z "$OPTIMIZATION"  ]; then
        OPTIMIZATIONLEVEL="2"
    else
        OPTIMIZATIONLEVEL="$OPTIMIZATION"
    fi

    rm -rf "$PREFIXDIR"

    ( cd source
      sed -i "s/^MYCFLAGS.*/MYCFLAGS=-O$OPTIMIZATIONLEVEL $DEBUGFLAG -fPIC/g" config
      sed -i "151 c\
            INSTALL_ROOT=$PREFIXDIR" config
      make
      make so
      make install
      make soinstall
      rm -rf "${PREFIXDIR:?}"/{lib/*.a,man})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYRIGHT "$PREFIXDIR"/lib/LICENSE-lua50.txt

echo "Lua for $MULTIARCHNAME is ready."
