Website
=======
http://www.lua.org/

License
=======
MIT license (see the file source/COPYRIGHT)

Version
=======
5.0.3

Source
======
lua-5.0.3.tar.gz (md5: feee27132056de2949ce499b0ef4c480)
